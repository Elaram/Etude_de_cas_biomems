function [ Pj ] = pressureIntersection( pump, RBV, TV, FW)
% Calculer la pression aux intersections du syst�me en fonction de la
% branche ou le vidage s'effectue

Pj = ( RBV.p*FW.R + pump.p*(TV.R + RBV.R) ) ...
     / (FW.R + TV.R + RBV.R); 

end

