clc; clear all; close all;

%% PARAM�TRE DES MAT�RIAUX

water.gamma = 72e-3; % N/m (tension de surface)
water.theta_backward = deg2rad([31 31 31 89]); % degr�s
water.theta_foward = deg2rad([45 45 45 114]); % degr�s
water.eta = 1.002e-3; % Pa*s (viscosit�)

%% PARAM�TRES G�OM�TRIQUES

% Pompe
pump.h = 100e-6; % en m�tres
pump.w = 200e-6; % en m�tres
pump.N = 12; % Nombre de branches

% Trigger valve
TV.h = 50e-6; % Dimensions minimales pour bon yield selon l'article
TV.w = 200e-6; % Dimensions minimales pour bon yield selon l'article
TV.L = 400e-6;

% Retention burst valves  (valeur en m�tres sauf specifications contraires)
RBV1.h = 450e-6;
RBV1.w = 800e-6; 
RBV1.L = 1000e-6; 

RBV2.h = 450e-6; 
RBV2.w = 670e-6; 
RBV2.L = 1000e-6; 

RBV3.h = 450e-6;
RBV3.w = 480e-6;
RBV3.L = 1000e-6; 

RBV4.h = 550e-6; 
RBV4.w = 380e-6;
RBV4.L = 1000e-6; 

RBV5.h = 450e-6; 
RBV5.w = 300e-6; 
RBV5.L = 1000e-6; 

%Canaux r�sistifs (r�servoirs)
R1.h = 960e-6; 
R1.w = 1000e-6;
R1.L = 6250e-6;

R2.h = 960e-6;
R2.w = 1000e-6;
R2.L = 6250e-6;

R3.h = 960e-6;
R3.w = 1000e-6;
R3.L = 6250e-6;

R4.h = 960e-6;
R4.w = 1000e-6;
R4.L = 6250e-6;

R5.h = 500e-6;   % serpentin?????
R5.w = 500e-6;
R5.L = 22000e-6;

FW.h = 300e-6;  % R�sistance principale????
FW.w = 300e-6;
FW.L = 45000e-6;


%% CALCUL DES PRESSSIONS

RBV1.p = laplacePressure(water, RBV1, 'receding'); % Calcule la burst pressure
RBV2.p = laplacePressure(water, RBV2, 'receding');
RBV3.p = laplacePressure(water, RBV3, 'receding');
RBV4.p = laplacePressure(water, RBV4, 'receding');

pump.p = laplacePressure(water, pump, 'advancing');   % ???? est-ce qu'on doit multiplier par (pump.N *) pour avoir la pression capilaire de la pompe? 

%% CALCUL DES R�SISTANCES

TV.R = resistance(water, TV); % m�me valeur pour toutes les branches
% RBV
RBV1.R = resistance(water, RBV1); 
RBV2.R = resistance(water, RBV2);
RBV3.R = resistance(water, RBV3); 
RBV4.R = resistance(water, RBV4);
% Reservoir
R1.R = resistance(water, R1); 
R2.R = resistance(water, R2); 
R3.R = resistance(water, R3); 
R4.R = resistance(water, R4); 
% R�sistance principale 
FW.R = resistance(water, FW); 

%% CALCUL DES PRESSIONS AUX INTERSECTIONS (LORS DES DIFF�RENTS VIDAGES)
% Vidage branche 1
vidage.premier_p = pressureIntersection(pump, RBV1, TV, FW);
% Vidage branche 2
vidage.deuxieme_p = pressureIntersection(pump, RBV2, TV, FW);
% Vidage branche 3
vidage.troisieme_p = pressureIntersection(pump, RBV3, TV, FW);
% Vidage branche 4 (serpentin) 
vidage.quatrieme_p = pressureIntersection(pump, RBV4, TV, FW);

%% Display imortant values

fprintf(' RBV1.p: %d\n RBV2.p: %d\n RBV3.p: %d\n RBV4.p: %d\n Pc: %d\n', ...
        RBV1.p, RBV2.p, RBV3.p, RBV4.p, pump.p);

